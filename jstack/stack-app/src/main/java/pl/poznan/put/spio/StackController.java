package pl.poznan.put.spio;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
public class StackController {

    private Stack stack;

    @GetMapping("/{element}")
    public void push(final @PathVariable String element) {
        stack.push(element);
    }

    @GetMapping
    public String pop() {
        return String.valueOf(stack.pop());
    }
}
